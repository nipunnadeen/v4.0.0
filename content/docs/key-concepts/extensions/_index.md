---
bookCollapseSection: true
weight: 8
---


# Extensions

## Device Agents

Device agent is a software program installed on the hardware device that enables communication between the hardware device and Entgra IoT Server. 

## Transport Extensions

Transport extensions enable you to establish a new communication channel between a device and Entgra IoT Server. 

## Authentication Extensions

By default WOS2 IoT Server supports OAuth, basic auth, mutual SSL and certificate-based authentication mechanisms. If the new device types require some other authentication mechanism, authentication extensions can be used for this purpose.

## UI Extensions

This helps you to customize UIs of the new device type. 