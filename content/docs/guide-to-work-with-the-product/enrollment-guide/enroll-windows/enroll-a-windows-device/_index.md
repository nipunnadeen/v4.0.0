---
bookCollapseSection: true
weight: 2
---

# Enroll a Windows device

{{< hint info >}}
<strong>Pre-requisites</strong>
<ul style="list-style-type:disc;">
    <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
    <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
    <li>Click on Add device (https://{IP}:{port}/devicemgt/device/enroll)</li>
    <li>Click on Windows from "DEVICE TYPES"</li>
</ul>
{{< /hint >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/6BYVQ5wvEpc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Add host configuration to the windows host files</li>
    <li>Add the server certificate to "trusted root certification authorities"</li>
    <li>Go to "access to work or school" in windows settings</li>
    <li>Click "enroll only in device management"</li>
    <li>Input admin@
        <server address> in the pop up window</li>
    <li>Login with admin credentials to enroll the device in the next pop up window</li>
</ul>