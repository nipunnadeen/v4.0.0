---
bookCollapseSection: true
weight: 4
---

# Access Point Name

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configurations can be used to configure APN(Access Point Name) on an Android device. This policy will support with the Android 9.0 onwards. This will not work with the below versions. And also Agent must be the device owner to activate this policy.



<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Name</strong></td>
            <td>String:The name to set for the APN This value may be null.<br>Eg:Entgra</td>
        </tr>
        <tr>
            <td><strong>Entry Name</strong></td>
            <td>String:The entry name to set for the APN This value may be null.<br>Eg:entgra</td>
        </tr>
        <tr>
            <td><strong>Proxy Address </strong></td>
            <td>String: the proxy address to set for the APN This value may be null.<br>Eg:[ 192.168.8.1 ]</td>
        </tr>
        <tr>
            <td><strong>Proxy Port </strong></td>
            <td>int: the proxy port to set for the APN<br>Eg:[ Target port 0-65535 ]</td>
        </tr>
        <tr>
            <td><strong>Username </strong></td>
            <td>String: the APN username to set for the APN This value may be null.</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>String: the APN password to set for the APN This value may be null.</td>
        </tr>
        <tr>
            <td><strong>Server </strong></td>
            <td>String: the server set for the APN This value may be null.</td>
        </tr>
        <tr>
            <td><strong>MMSC</strong></td>
            <td>Uri: the MMSC Uri to set for the APN This value may be null.<br>Eg:[ 192.168.8.1 ]</td>
        </tr>
        <tr>
            <td><strong>MMS Proxy Address</strong></td>
            <td>String: the MMS proxy address to set for the APN This value may be null.<br>Eg:[ 192.168.8.1 ]</td>
        </tr>
        <tr>
            <td><strong>MMS Proxy Port  </strong></td>
            <td>int: the MMS proxy port to set for the APN.<br>Eg:[ Target port 0-65535 ]</td>
        </tr>
        <tr>
            <td><strong>MCC </strong></td>
            <td>int: the Mobile Country Code to set for the APN.<br>Eg:413 </td>
        </tr>
        <tr>
            <td><strong>MNC </strong></td>
            <td>int: the Mobile Network Code  to set for the APN.<br>Eg:02 </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Authentication Type </strong>
                    <br>int: the authentication type to set for the APN Value</center>
            </td>
        </tr>
        <tr>
            <td><strong>NONE </strong></td>
            <td>int: default Authentication Type for the APN.<br>Constant Value: 0 (0x00000000) </td>
        </tr>
        <tr>
            <td><strong>PAP</strong></td>
            <td>int: Password Authentication Protocol for the APN.<br>Constant Value: 1 (0x00000001)</td>
        </tr>
        <tr>
            <td><strong>CHAP</strong></td>
            <td>int: Challenge Handshake Authentication Protocol for the APN.<br>Constant Value: 2 (0x00000002)</td>
        </tr>
        <tr>
            <td><strong>PAP_OR_CHAP</strong></td>
            <td>int: Authentication type for PAP or CHAP for the APN.<br>Constant Value: 3 (0x00000003)</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>APN Type</strong>
                    <br>int: Apn types are usage categories for an APN entry. One APN entry may support multiple APN types.</center>
            </td>
        </tr>
        <tr>
            <td><strong>DEFAULT</strong></td>
            <td>int: APN type for default data traffic. <br>Constant Value: 17 (0x00000011)</td>
        </tr>
        <tr>
            <td><strong>CBS</strong></td>
            <td>int: Carrier Branded Services for the APN.<br>Constant Value: 128 (0x00000080)</td>
        </tr>
        <tr>
            <td><strong>DUN</strong></td>
            <td>int: Dial Up Networking bridge for the APN.<br>Constant Value: 8 (0x00000008)</td>
        </tr>
        <tr>
            <td><strong>IMS</strong></td>
            <td>int: IP Multimedia Subsystem for the APN.<br>Constant Value:64 (0x00000040)</td>
        </tr>
        <tr>
            <td><strong>MMS</strong></td>
            <td>int: Multimedia Messaging Service for the APN.<br>Constant Value: 2 (0x00000002)</td>
        </tr>
        <tr>
            <td><strong>SUPL</strong></td>
            <td>int: APN type for SUPL assisted GPS.<br> Constant Value: 4 (0x00000004)</td>
        </tr>
        <tr>
            <td><strong>IA</strong></td>
            <td>int: APN type for IA Initial Attach APN.<br>Constant Value: 256 (0x00000100)</td>
        </tr>
        <tr>
            <td><strong>HIPRI</strong></td>
            <td>int: APN type for HiPri traffic.<br>Constant Value: 16 (0x00000010)</td>
        </tr>
        <tr>
            <td><strong>FOTA</strong></td>
            <td>int: APN type for accessing the carrier's FOTA portal, used for over the air updates.<br>Constant Value: 32 (0x00000020)</td>
        </tr>
        <tr>
            <td><strong>EMERGENCY</strong></td>
            <td>int: used for access to carrier services in an emergency call situation.<br>Constant Value: 512 (0x00000200)</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>APN Protocol</strong>
                    <br>int: Sets the protocol to use to connect to this APN</center>
            </td>
        </tr>
        <tr>
            <td><strong>IPV4V6</strong></td>
            <td>int: Virtual PDP type introduced to handle dual IP stack UE capability.<br>Constant Value: 2 (0x00000002)</td>
        </tr>
        <tr>
            <td><strong>IP</strong></td>
            <td>int: Internet protocol.<br>Constant Value: 0 (0x00000000)</td>
        </tr>
        <tr>
            <td><strong>IPV6</strong></td>
            <td>int: Internet protocol, version 6.<br>Constant Value: 1 (0x00000001)</td>
        </tr>
        <tr>
            <td><strong>PPP</strong></td>
            <td>int: Point to point protocol.<br>Constant Value: 3 (0x00000003)</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>APN Roaming Protocol</strong>
                    <br>int: Sets the protocol to use to connect to this APN when the device is roaming</center>
            </td>
        </tr>
        <tr>
            <td><strong>IPV4V6</strong></td>
            <td>int: Virtual PDP type introduced to handle dual IP stack UE capability.<br>Constant Value: 2 (0x00000002)</td>
        </tr>
        <tr>
            <td><strong>IP</strong></td>
            <td>int: Internet protocol.<br>Constant Value: 0 (0x00000000)</td>
        </tr>
        <tr>
            <td><strong>IPV6</strong></td>
            <td>int: Internet protocol, version 6.<br>Constant Value: 1 (0x00000001)</td>
        </tr>
        <tr>
            <td><strong>PPP</strong></td>
            <td>int: Point to point protocol.<br>Constant Value: 3 (0x00000003)</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>APN Bearer</strong>
                    <br>int: Sets Radio Technology (Network Type) info for this APN.</center>
            </td>
        </tr>
        <tr>
            <td><strong>UNSPECIFIED</strong></td>
            <td>int: APN type for default data traffic.</td>
        </tr>
        <tr>
            <td><strong>LTE</strong></td>
            <td>int: Long Term Evolution is a standard in the communication of Mobile Phones used for data transfer</td>
        </tr>
        <tr>
            <td><strong>HSPAP</strong></td>
            <td>int: Dial Up Networking bridge for the APN.<br>Constant Value: 8 (0x00000008)</td>
        </tr>
        <tr>
            <td><strong>HSPA</strong></td>
            <td>int:High Speed Packet Access</td>
        </tr>
        <tr>
            <td><strong>HSUPA</strong></td>
            <td>int: High Speed Uplink Packet Access</td>
        </tr>
        <tr>
            <td><strong>HSDPA</strong></td>
            <td>int: High Speed Download/Upload Packet Access</td>
        </tr>
        <tr>
            <td><strong>UMTS</strong></td>
            <td>int: Universal Mobile Telecommunications System</td>
        </tr>
        <tr>
            <td><strong>EDGE</strong></td>
            <td>int: Enhanced Data for GSM Evolution</td>
        </tr>
        <tr>
            <td><strong>GPRS</strong></td>
            <td>int: General Packet Radio Service.<br>Constant Value: 32 (0x00000020)</td>
        </tr>
        <tr>
            <td><strong>eHRPD</strong></td>
            <td>int: Evolved High-Rate Packet Data</td>
        </tr>
        <tr>
            <td><strong>EVDO_0</strong></td>
            <td>int: Initial design of Evolution Data Optimized</td>
        </tr>
        <tr>
            <td><strong>EVDO_A</strong></td>
            <td>int: Several additions to the EVDO_0</td>
        </tr>
        <tr>
            <td><strong>EVDO_B</strong></td>
            <td>int: Multi-carrier evolution of the EVDO_A specification</td>
        </tr>
        <tr>
            <td><strong>1xRTT</strong></td>
            <td>int: Single carrier (1x) radio transmission technology</td>
        </tr>
        <tr>
            <td><strong>GSM</strong></td>
            <td>int: Global System for Mobile Communications</td>
        </tr>
        <tr>
            <td><strong>IWLAN</strong></td>
            <td>int: Industrial Wireless Local Area Network</td>
        </tr>
        <tr>
            <td><strong>APN Enable/Disable</strong></td>
            <td>boolean: the current status to set for this APN</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Mobile Virtual Network Operator Type </strong>
                    <br>int: Sets the Mobile Virtual Network Operator match type for this APN.</center>
            </td>
        </tr>
        <tr>
            <td><strong>SPN</strong></td>
            <td>int: MVNO type for service provider name.<br>Constant Value: 0 (0x00000000)</td>
        </tr>
        <tr>
            <td><strong>GID</strong></td>
            <td>int: MVNO type for group identifier level 1.<br>Constant Value: 2 (0x00000002)</td>
        </tr>
        <tr>
            <td><strong>ICCID</strong></td>
            <td>int: MVNO type for  Integrated Circuit Card ID.<br>Constant Value: 3 (0x00000003)</td>
        </tr>
        <tr>
            <td><strong>IMSI</strong></td>
            <td>int:MVNO type for International Mobile Subscriber Identity<br>Constant Value: 1 (0x00000001)</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}

