---
bookCollapseSection: true
weight: 15
---

# App Usage Time Configuration 

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This policy is for monitor the app usage time. It’s a generic policy which can monitor different type of usages time. 
Once this policy is applied by giving app package names, allowed time and the period time the policy will monitor whether user use the given apps violently.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Usage time type</strong></td>
            <td>Select the type of the usage time.</td>
        </tr>
        <tr>
            <td colspan = "2"><strong><center>Application List & Time List</center></strong></td>
        </tr>
        <tr>
            <td><strong>Package Name</strong></td>
            <td>Eg: [ com.google.android.gm ]</td>
        </tr>
        <tr>
            <td><strong>Allow Time Type</strong></td>
            <td>Drop down values Eg: Minute,Hour,...</td>
        </tr>
        <tr>
            <td><strong>Allow Time</strong></td>
            <td>Eg: [ time ]</td>
        </tr>
        <tr>
            <td><strong>Period Time Type</strong></td>
            <td>Drop down values Eg: Minute,Hour,...</td>
        </tr>
        <tr>
            <td><strong>Period Time</strong></td>
            <td>Eg: [ time ]</td>
        </tr>                                
    </tbody>
</table>


{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}