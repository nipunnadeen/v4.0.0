---
bookCollapseSection: true
weight: 14
---

# Display Message Configuration

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This policy can be used to show the messages on the devices.To display the Lock screen message the 
device should have the device owner but the device owner does not need to display the long support 
message and the short support message.
 

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Lock Screen Message</strong></td>
            <td><i>Lock screen message works only for device owner. Once this message is applied to device 
            it will show on the device lock screen and user can’t change it from the setting..</i></td>
        </tr>
        <tr>
            <td><strong>Setting App Support Message</strong></td>
            <td><i>Once this message is applied to devices the message will be displayed to the user in the device 
                administrators settings screen.</i></td>
        </tr>
        <tr>
            <td><strong>Disabled Setting Support Message</strong></td>
            <td><i>Once this message is applied to devices the message will be displayed to the user in settings 
                   screens where functionality has been disabled by the admin.The message maximum length is 200 characters.</i></td>
        </tr>
    </tbody>
</table>


{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}